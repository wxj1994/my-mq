package com.richinfo.appoint;
/**
 * 目标对象(target)
 * 
* Created by WangXJ
* 2019-03-07 14:27
*/
public class HelloServiceImpl implements HelloService {

	@Override
	public void sayHello(String name) {
		if (name == null || name.trim() == "") {
			throw new RuntimeException("参数为空");
		}
		System.out.println("hello " + name);
	}

}
