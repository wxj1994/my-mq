package com.richinfo.appoint;
/**
 * 启动类
 * 
* Created by WangXJ
* 2019-03-07 15:30
*/
public class Application {

	public static void main(String[] args) {
		HelloService helloService = new HelloServiceImpl();
		// 按约定获取 proxy
		HelloService proxy = (HelloService)ProxyBean.getProxyBean(helloService, new MyInterceptor());
		proxy.sayHello("World");
		System.out.println("================= Name is null! ===================");
		proxy.sayHello(null);
	}

}
