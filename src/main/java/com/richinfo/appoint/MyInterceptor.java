package com.richinfo.appoint;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 * 我的拦截器
 * 
* Created by WangXJ
* 2019-03-07 15:01
*/
public class MyInterceptor implements Interceptor{

	@Override
	public boolean before() {
		System.out.println("Before ......");
		return false;
	}

	@Override
	public void after() {
		System.out.println("After ......");
	}

	@Override
	public Object around(Invocation invocation) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("Around before ......");
		Object obj = invocation.proceed();
		System.out.println("Around after ......");
		return obj;
	}

	@Override
	public void afterReturning() {
		System.out.println("AfterReturning ......");
	}

	@Override
	public void afterThrowing() {
		System.out.println("AfterThrowing ......");
	}

	@Override
	public boolean userAround() {
		return true;
	}

}
