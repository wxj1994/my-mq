package com.richinfo.appoint;

import java.lang.reflect.InvocationTargetException;

/**
 * 拦截器接口
 * 
* Created by WangXJ
* 2019-03-07 14:29
*/
public interface Interceptor {
	
	// 事前方法
	boolean before();
	
	// 事后方法
	void after();
	
	/**
	 *  取代原有事件方法
	 * @param invocation -- 回调参数，可以通过它的proceed方法，回调原有事件
	 * @return 原有事件返回对象
	 * @author WangXJ
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @date 2019-03-07 14:46
	 */
	Object around(Invocation invocation) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	
	// 事后返回方法 (事件没有抛出异常时执行)
	void afterReturning();
	
	// 事后异常方法 (当事件抛出异常时执行)
	void afterThrowing();
	
	// 是否使用 around 方法取代原有方法
	boolean userAround();
}
