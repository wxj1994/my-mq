package com.richinfo.appoint;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 反射类实体
 * 
* Created by WangXJ
* 2019-03-07 14:36
*/
public class Invocation {

	private Object target;
	
	private Method method;
	
	private Object[] params;

	public Invocation(Object target, Method method, Object[] params) {
		super();
		this.target = target;
		this.method = method;
		this.params = params;
		
	}
	
	// 反射方式
	public Object proceed() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return method.invoke(target, params);
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}
	
	
}
