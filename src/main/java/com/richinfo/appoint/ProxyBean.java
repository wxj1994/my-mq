package com.richinfo.appoint;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 代理对象
 * 
* Created by WangXJ
* 2019-03-07 16:40
*/
public class ProxyBean implements InvocationHandler{
	// 被代理的对象
	private Object target = null;
	// 拦截器
	private Interceptor interceptor = null;

	/**
	 * 绑定代理对象
	 * @param target-被代理的对象
	 * @param interceptor-拦截器
	 * @return proxy-代理对象
	 */
	public static Object getProxyBean(Object target, Interceptor interceptor) {
		ProxyBean proxyBean = new ProxyBean();
		// 保存被代理的对象
		proxyBean.target = target;
		// 保存拦截器
		proxyBean.interceptor = interceptor;
		// 生成代理对象 (proxy)
		Object proxy = Proxy.newProxyInstance(target.getClass().getClassLoader(),
											  target.getClass().getInterfaces(),
											  proxyBean);
		// 返回代理对象
		return proxy;
	}
	
	/**
	 * 处理代理对象方法的逻辑
	 * @param proxy-代理对象
	 * @param method-当前方法
	 * @param args-运行参数
	 * @return 方法调用结果
	 * @throws Throwable-异常
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// 异常标识
		boolean exceptionFlag = false;
		Invocation invocation = new Invocation(target, method, args);
		Object retObj = null;
		// 事前方法
		this.interceptor.before();
		try {
			// 是否用代理对象的方法代替原对象的方法
			if (this.interceptor.userAround()) {
				retObj = this.interceptor.around(invocation);
			} else {
				retObj = method.invoke(target, args);
			}
		} catch (Exception ex) {
			// 产生异常
			exceptionFlag = true;
		}
		this.interceptor.after();
		if (exceptionFlag) {
			// 抛出异常后方法
			this.interceptor.afterThrowing();
		} else {
			// 事后方法 (没抛出异常)
			this.interceptor.afterReturning();
			// 原有对象方法的返回值
			return retObj;
		}
		return null;
	}

}
