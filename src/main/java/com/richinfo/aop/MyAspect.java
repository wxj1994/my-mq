package com.richinfo.aop;

import javax.annotation.PreDestroy;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * 自定义-切面
 * 
* Created by WangXJ
* 2019-03-11 10:48
*/

@Aspect
public class MyAspect {
	
	// 切点
	@Pointcut("execution(*  com.richinfo.aop.UserServiceImpl.printUser(..))")
	public void pointCut() {
		
	}
	
	@Before("pointCut()")
	public void before() {
		System.out.println("Before ......");
	}
	
	@After("pointCut()")
	public void after() {
		System.out.println("After ......");
	}
	
	@AfterReturning("pointCut()")
	public void afterReturning() {
		System.out.println("AfterReturning ......");
	}
	
	@AfterThrowing("pointCut()")
	public void afterThrowing() {
		System.out.println("AfterThrowing ......");
	}
}
