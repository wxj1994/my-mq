package com.richinfo.aop;


/**
* Created by WangXJ
* 2019-03-11 10:41
*/
public class UserServiceImpl implements UserService {

	@Override
	public void printUser(User user) {
		if (user == null) {
			throw new RuntimeException("检查用户参数是否为空 ......");
		}
		System.out.println("id = " + user.getId());
		System.out.println("username = " + user.getUserName());
		System.out.println("note = " + user.getNote());
	}

}
