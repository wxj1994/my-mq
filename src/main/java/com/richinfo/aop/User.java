package com.richinfo.aop;
/**
* Created by WangXJ
* 2019-03-11 10:40
*/
public class User {
	
	private String id;
	
	private String userName;
	
	private String note;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	
}
