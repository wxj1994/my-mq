package com.richinfo.aop;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户控制器
 * 
* Created by WangXJ
* 2019-03-11 14:17
*/
@RequestMapping("/user")
@RestController
public class UserController {

	// 注入用户服务
	@Resource
	private UserService userService;
	
	// 定义请求
	@GetMapping("/print")
	public User printUser(String id, String userName, String note) {
		User user = new User();
		user.setId(id);
		user.setUserName(userName);
		user.setNote(note);
		userService.printUser(user);
		return user;
	}
}
