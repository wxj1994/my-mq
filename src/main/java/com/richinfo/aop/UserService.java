package com.richinfo.aop;
/**
* Created by WangXJ
* 2019-03-11 10:39
*/
public interface UserService {
	
	void printUser(User user);
}
