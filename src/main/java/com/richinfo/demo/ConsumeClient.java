package com.richinfo.demo;
/**
 * 消费者
 * 
* Created by WangXJ
* 2019-02-20 15:10
*/
public class ConsumeClient {

	public static void main(String[] args) throws Exception {
		MqClient client = new MqClient();
		String message = client.consume();
		System.out.println("获取的消息为: " + message);
	}

}
