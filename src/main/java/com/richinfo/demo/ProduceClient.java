package com.richinfo.demo;
/**
 * 生产者
 * 
* Created by WangXJ
* 2019-02-20 14:59
*/
public class ProduceClient {

	public static void main(String[] args) throws Exception {
		MqClient client = new MqClient();
		client.produce("Hello 蒜头王八");
	}

}
